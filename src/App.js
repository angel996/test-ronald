import {useEffect, useState} from 'react';
import axios from 'axios';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import DeleteIcon  from '@mui/icons-material';
import Modal from '@mui/material/Modal';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import {makeStyles} from '@mui/styles';

const baseUrl='https://my.api.mockaroo.com/users.json?key=e877c7c0'
const eliminarUsuarios=''
const actualizarUsuarios=''

const useStyles = makeStyles((theme) => ({
  modal: {
    position: 'absolute',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)'
  },
  iconos:{
    cursor: 'pointer'
  }, 
  inputMaterial:{
    width: '100%'
  }
}));

function App(){
  const styles= useStyles();
  const[data, setData] = useState([]);
  const[modalInsertar, setModalInsertar] = useState(false);
  const [modalEditar, setModalEditar]=useState(false);
  const [modalEliminar, setModalEliminar]=useState(false);

  const [usuarioSeleccionada, setUsuarioSeleccionada]=useState({
    id: '',
    firstName:'',
    lastName: '',
    UserName: '',
    Email: ''
  })

  const handleChange=e=>{
    const {name, value}=e.target;
    setUsuarioSeleccionada(prevState=>({
      ...prevState,
      [name]: value
    }))
    console.log(usuarioSeleccionada);
  }

  const peticionGet=async()=>{
     await axios.get(baseUrl).then(response =>{
       console.log(response.data);
      setData(response.data);
    })
  }
  const peticionPost=async()=>{
    await axios.post(baseUrl, usuarioSeleccionada)
    .then(response=>{
      setData(data.concat(response.data))
      abrirCerrarInsertar()
    })
  }
  const peticionPut=async()=>{
    await axios.put(baseUrl+usuarioSeleccionada.id, usuarioSeleccionada)
    .then(response=>{
      var dataNueva=data;
      dataNueva.map(user=>{
        if(usuarioSeleccionada.id===user.id){
          user.firstName=usuarioSeleccionada.firstName;
          user.UserName=usuarioSeleccionada.lastName;
          user.lastName=usuarioSeleccionada.UserName;
          user.Email=usuarioSeleccionada.Email;
        }
      })
      setData(dataNueva);
      abrirCerrarEditar();
    })
  }

  const peticionDelete=async()=>{
    await axios.delete(baseUrl+usuarioSeleccionada.id)
    .then(response=>{
      setData(data.filter(user=>user.id!==usuarioSeleccionada.id));
      abrirCerrarEliminar();
    })
  }

  const abrirCerrarInsertar = () => {
    setModalInsertar(!modalInsertar);
  }
  const abrirCerrarEditar=()=>{
    setModalEditar(!modalEditar);
  }

  const abrirCerrarEliminar=()=>{
    setModalEliminar(!modalEliminar);
  }

  const seleccionarUsuario=(usuario, caso)=>{
    setUsuarioSeleccionada(usuario);
    (caso==='Editar')?abrirCerrarEditar():abrirCerrarEliminar()
  }

useEffect(async()=>{
  await peticionGet();
}, [])

const bodyInsertar=(
  <div className={styles.modal}>
    <h3>Agregar Nueva Consola</h3>
    <TextField name="firstName" className={styles.inputMaterial} label="firstName" onChange={handleChange}/>
    <br />
    <TextField name="lastName" className={styles.inputMaterial} label="lastName" onChange={handleChange}/>
    <br />
    <TextField name="UserName" className={styles.inputMaterial} label="UserName" onChange={handleChange}/>
    <br />
    <TextField name="Email" className={styles.inputMaterial} label="Email" onChange={handleChange}/>
    <br /><br />
    <div align="right">
      <Button color="primary" onClick={()=>peticionPost()}>Insertar</Button>
      <Button onClick={()=>abrirCerrarInsertar()}>Cancelar</Button>
    </div>
  </div>
)

const bodyEditar=(
  <div className={styles.modal}>
    <h3>Editar Consola</h3>
    <TextField name="firstName" className={styles.inputMaterial} label="firstName" onChange={handleChange} value={usuarioSeleccionada && usuarioSeleccionada.firstName}/>
    <br />
    <TextField name="lastName" className={styles.inputMaterial} label="lastName" onChange={handleChange} value={usuarioSeleccionada && usuarioSeleccionada.lastName}/>
    <br />
    <TextField name="UserName" className={styles.inputMaterial} label="UserName" onChange={handleChange} value={usuarioSeleccionada && usuarioSeleccionada.UserName}/>
    <br />
    <TextField name="Email" className={styles.inputMaterial} label="Email" onChange={handleChange} value={usuarioSeleccionada && usuarioSeleccionada.Email}/>
    <br /><br />
    <div align="right">
      <Button color="primary" onClick={()=>peticionPut()}>Editar</Button>
      <Button onClick={()=>abrirCerrarEditar()}>Cancelar</Button>
    </div>
  </div>
)

const bodyEliminar=(
  <div className={styles.modal}>
    <p>Estás seguro que deseas eliminar la consola <b>{usuarioSeleccionada && usuarioSeleccionada.firstName}</b> ? </p>
    <div align="right">
      <Button color="secondary" onClick={()=>peticionDelete()} >Sí</Button>
      <Button onClick={()=>abrirCerrarEliminar()}>No</Button>

    </div>

  </div>
)

  return(
    <div className="App">
      <br />
       <Button onclick={abrirCerrarInsertar}>Insertar</Button>
      <br />
      <TableContainer>
        <Table>
           <TableHead>
             <TableRow>
               <TableCell>Id</TableCell>
               <TableCell>FirstName</TableCell>
               <TableCell>LastName</TableCell>
               <TableCell>Username</TableCell>
               <TableCell>Email</TableCell>
               <TableCell>Acciones</TableCell>
             </TableRow>
           </TableHead>
           <TableBody>
             {data.map(usuario => {
               <TableRow key={usuario.id}>
                 <TableCell>{usuario.id}</TableCell>
                 <TableCell>{usuario.FirstName}</TableCell>
                 <TableCell>{usuario.LastName}</TableCell>
                 <TableCell>{usuario.UserName}</TableCell>
                 <TableCell>{usuario.Email}</TableCell>
                 <TableCell>
                  <DeleteIcon />
                </TableCell>
               </TableRow>
             })}
           </TableBody>
        </Table>
      </TableContainer>
      <Modal
     open={modalInsertar}
     onClose={abrirCerrarInsertar}>
        {bodyInsertar}
     </Modal>

     <Modal
     open={modalEditar}
     onClose={abrirCerrarEditar}>
        {bodyEditar}
     </Modal>

     <Modal
     open={modalEliminar}
     onClose={abrirCerrarEliminar}>
        {bodyEliminar}
     </Modal>
    </div>
  );
}

export default App;